# Документирование решений в проекте

* **Status:** proposed
* **Last Updated:** 2023-01-18
* **Objective:** выбрать способ докуменитирования решений

## Context & Problem Statement

2-3 sentences explaining the problem and why it's challenging.

## Priorities & Constraints <!-- optional -->

* единый простой формат для всех решений
* версионирование и структурирование
* возможность добавлять графические артефакты в решения

## Considered Options

Стукура:

* Option 1: [ADR](https://adr.github.io/)
* Option 2: [DecisionCapture](https://schubmat.github.io/DecisionCapture/)
* Option 3: [Y-statement](https://medium.com/olzzio/y-statements-10eb07b5a177)
* Option 4: [ADR cards](https://cards42.org/#adr)
* Option 4: [MADR](https://adr.github.io/madr/)

Формат:
* Option 1: MD
* Option 2: Confluence

## Decision

Chosen option [Option 1: ADR] + [Option 1: MD]

![roadmap](http://www.plantuml.com/plantuml/proxy?cache=no&src=https://gitlab.com/bydesp/decisions/-/raw/main/example.puml)

[justification]

### Expected Consequences <!-- optional -->

* List of unrelated outcomes this decision creates

### Revisiting this Decision <!-- optional -->

### Research <!-- optional -->

* Resources reviewed as part of making this decision

## Links

* Related PRs
* Related User Journeys
